/* dockWindow.js
 *
 * Copyright 2022 Sundeep Mediratta
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';
import { CompositedWindow } from './compositedWindow.js';

const DockWindow = GObject.registerClass({
    GTypeName: 'DockWindow',
    Template: 'resource:///org/gnome/HickoryDickoryDock/Widgets/dockwindow.cmb.ui'
}, class DockWindow extends Gtk.ApplicationWindow {
    constructor(application) {
        super({application});
    }
});


export const dockWindow = class dockWindow {
      constructor(application) {
          this._window = new CompositedWindow(application);
          return this._window;
      }
};            
